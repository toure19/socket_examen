import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Thread_Server_Adeu extends Thread {

	String name;
	Socket socket;

	//CONSTRUCTOR
	public Thread_Server_Adeu(String name, Socket skClient) {
		this.setName(name);
		this.socket = skClient;
	}

	
	public void run(){
		String requestData = null, responseData = null;
		int i=1;
		System.out.println(this.getName()+": Attending the Client ");

		try {
			// Input socket creation
			InputStream in = socket.getInputStream();
			// getting data from Client socket
			DataInputStream inStream = new DataInputStream(in);

			// OutputStream creation
			OutputStream out = socket.getOutputStream();
			// setting a new DataOutputStream to send data to Client
			DataOutputStream outStream = new DataOutputStream(out);

			do{
				// Printing data received
				requestData = inStream.readUTF();

				System.out.println("["+ this.name +"] " + requestData);

				if(requestData.contains("adeu"))
					responseData = "Finishing connection";
				else 
					responseData = "Response "+ i++ + "from Server";

				// Writing on Client socket
				outStream.writeUTF(responseData);
			} while(!requestData.contains("adeu"));

			System.out.println("[Server] Closing client connection\n ---\n");
			//}////end while infinite loop

			// Close socket
			if (socket != null && !socket.isClosed()) {
				if (!socket.isInputShutdown()) {
					socket.shutdownInput();
				}
				if (!socket.isOutputShutdown()) {
					socket.shutdownOutput();
				}
				socket.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}