import java.io.*;
import java.net.*;
import java.util.Scanner;

public class TCPClient_adeu {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String requestData = null, responseData = null;
		// nameServer and port

		InetAddress hostServer = InetAddress.getByName(args[0]);
		int port = 9999;

		// Socket creation
		Socket skClient = new Socket(hostServer, port);

		// Output socket creation
		OutputStream out = skClient.getOutputStream();
		//setting a new DataOutputStream to send data to Server 
		DataOutputStream outStream= new DataOutputStream(out);

		// Input socket creation
		InputStream in = skClient.getInputStream();
		// getting data from socket
		DataInputStream inStream = new DataInputStream(in);

		System.out.println("Connected to Server ");

		do{
			//Writing on Server socket
			responseData = getCadena();
			outStream.writeUTF(responseData);

			// Printing data received
			requestData = inStream.readUTF();
			System.out.println("[Server] "+ requestData);

			if(responseData.contains("adeu"))
				System.out.println("[Client] Finishing connection");

		} while(!responseData.contains("adeu"));


		// Close socket
		if (skClient != null && !skClient.isClosed()) {
			if (!skClient.isInputShutdown()) {
				skClient.shutdownInput();
			}
			if (!skClient.isOutputShutdown()) {
				skClient.shutdownOutput();
			}
			skClient.close();
		}

	}

	// --------------------------------------------------

	static String getCadena() {
		String cadena = null;

		Scanner s = new Scanner(System.in);
		System.out.print("Cadena a enviar:");
		cadena = s.nextLine();

		return cadena;
	}

}