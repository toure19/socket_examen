import java.io.*;
import java.net.*;

public class TCPServer_Thread_Adeu {

	public static void main(String[] args) throws IOException {
		int port = 9999, i = 1;
		Socket skClient = null;
		//boolean iterar = true;
		Thread_Server_Adeu fil = null;

		// ServerSocket creation
		ServerSocket skServidor = new ServerSocket(port);
		System.out.println("[Server] Listening on port " + port);

		System.out.println("[Server] Waiting requests from clients ...");
		
		// 3 loops
		while (i < 3) {
			
			// Socket waiting until receiving a new request
			// skClient handles the request
			skClient = skServidor.accept();

			System.out.println("[Server] Waiting a new Client... ");

			fil = new Thread_Server_Adeu("FilTCP"+i, skClient);
			fil.start();
			//finishing loop
			i++;
		}////end while infinite loop

		System.out.println("[Server] Client served!");
		//Closing ServerSocket
		if(!skServidor.isClosed())
			skServidor.close();
	}

}