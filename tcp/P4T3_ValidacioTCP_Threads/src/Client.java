import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		/////////////////////////////////////////////////////////////////////////////////
		String cadenaAEnviar, cadenaRebuda;
		int portOnEnviar = 9999;
		boolean usuariValidat = false;
		DatagramPacket resp;
		try {
			Scanner s = new Scanner(System.in);
			InetAddress hostServer = InetAddress.getLocalHost();
			// Socket creation
			Socket skClient = new Socket(hostServer, portOnEnviar);
			System.out.println("CLIENT connection on Server port: "+portOnEnviar);

			//Getting Streams ********************************************************************
			// Output socket creation
			OutputStream out = skClient.getOutputStream();
			//setting a new DataOutputStream to send data to Server 
			DataOutputStream outStream= new DataOutputStream(out);

			// Input socket creation
			InputStream in = skClient.getInputStream();
			// getting data from socket
			DataInputStream inStream = new DataInputStream(in);

			//Getting USERNAME to SERVER   *************************************************************
			System.out.print("Usuari a enviar al servidor: ");
			//missage to SEND 
			cadenaAEnviar=s.nextLine()+ " ";

			//Sending USERNAME >>>>>>>>>>>>>>>>
			outStream.writeUTF(cadenaAEnviar);

			//Loop until valid username
			do {	
				// Getting USERNAME
				cadenaRebuda = inStream.readUTF();

				//System.out.println("Cadena enviada pel SERVIDOR: "+cadenaRebuda);
				if (cadenaRebuda.contains("NO existeix")) { //WRONG username identified
					System.out.println(">"+cadenaRebuda);
					//Asking for sending new attempt
					System.out.print("Proporciona un usuari vàlid: ");
					cadenaAEnviar = s.nextLine()+" \n";
					//Sending response to Client *****************************************************
					outStream.writeUTF(cadenaAEnviar);

				}else {	//RIGHT username identified, so asking for PASSWORD
					System.out.print("Contrassenya per usuari "+ cadenaAEnviar.substring(0, cadenaAEnviar.length()-2) +": ");
					usuariValidat = true;
					cadenaAEnviar = "PASSWD "+ s.nextLine()+" \n";	
				}
			}while(!usuariValidat);


			//System.out.println("Cadena a enviar pel CLIENT:"+cadenaAEnviar);

			//missatge = cadenaAEnviar.getBytes();
			//Sending PASSWORD to check  *************************************************************
			outStream.writeUTF(cadenaAEnviar);

			//Receiving a new packet,LOGIN checked   ******************************************************
			cadenaRebuda = inStream.readUTF();

			System.out.println(">"+cadenaRebuda);

			// Close socket
			if (skClient != null && !skClient.isClosed()) {
				if (!skClient.isInputShutdown()) {
					skClient.shutdownInput();
				}
				if (!skClient.isOutputShutdown()) {
					skClient.shutdownOutput();
				}
				skClient.close();
			}


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
