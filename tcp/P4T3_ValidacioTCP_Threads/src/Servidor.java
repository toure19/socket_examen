import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class Servidor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/////////////////////////////////////////////////////////////////////////////////
		int portEscolta = 9999;
		int i=0, TTL = 20000;
		Socket socketClient = null;
		filServidor fil = null;
		

		try {
			//Scanner s = new Scanner(System.in);
			System.out.println("[Server] Listening on port  "+ portEscolta);
			long tempsInicial = System.currentTimeMillis();

			// ServerSocket creation
			ServerSocket serversocket = new ServerSocket(portEscolta);//<--- LISTEN port 9999
			serversocket.setSoTimeout(TTL);

			do {
				// Socket waiting until receiving a new request
				// socketClient handles the request
				socketClient = serversocket.accept();

				fil = new filServidor(socketClient,i++);
				fil.start();

			}while (System.currentTimeMillis() - tempsInicial < TTL);



			System.out.println("\t\t["+Thread.currentThread().getName()+"] "+ "Tancant atenció peticions del SERVIDOR passats "+ TTL/1000 +" seg: ");
			//Closing ServerSocket
			if(!serversocket.isClosed())
				serversocket.close();
			
			fil.join();

		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			if(e instanceof SocketTimeoutException) 
				System.out.println("Timeout!!! Excepció pel venciment del temporitzador de "+ TTL/1000 + " segs");
			else
				e.printStackTrace();
		}
	}

}
