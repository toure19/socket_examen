import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class filServidor extends Thread{

	private Socket socket;


	//CONSTRUCTOR
	public filServidor(Socket socket, int i) {

		this.socket = socket;
		this.setName("filServidor "+i);
	}

	public void run() {
		String cadenaRebuda, cadenaAEnviar, contrassenya;
		String ValidUser = "laura", ValidPassword="l123";
		boolean loginSuccessfull = false;

		try {
			System.out.println("[Server. "+this.getName()+"] Connected, attending the Client ");

			// Input socket creation
			InputStream in = socket.getInputStream();
			// getting data from Client socket
			DataInputStream inStream = new DataInputStream(in);

			// OutputStream creation
			OutputStream out = socket.getOutputStream();
			// setting a new DataOutputStream to send data to Client
			DataOutputStream outStream = new DataOutputStream(out);

			do {
				// Printing data received
				cadenaRebuda = inStream.readUTF();
				System.out.println("[Server. "+this.getName()+"] Cadena enviada pel CLIENT: "+cadenaRebuda);


				if (!cadenaRebuda.contains("PASSWD")) {	//Obtaining USERNAME
					//Split data received and get USERNAME -> 1st vector = [0]
					if (cadenaRebuda.split(" ")[0].equals(ValidUser)) {  //username NOT EXISTS!!
						cadenaAEnviar = "Usuari existeix \n";

					}else {	//username EXISTS!!
						cadenaAEnviar = "Usuari NO existeix \n";
					}
					//System.out.println(cadenaAEnviar);

				}else { //Obtaining PASSWORD
					//Split data received and get PASSWORD  -> 2nd vector = [1]
					contrassenya = cadenaRebuda.split(" ")[1];
					//System.out.println(contrassenya);

					if (contrassenya.equals(ValidPassword)) {//RIGHT password
						cadenaAEnviar = "Usuari "+ValidUser+" validat correctament";
						loginSuccessfull = true;
					}else 	//WRONG password
						cadenaAEnviar = "Usuari "+ValidUser+" NO validat, contrassenya incorrecta";
				}
				System.out.println("[Server. "+this.getName()+"] " + cadenaAEnviar);

				// Writing on Client socket
				outStream.writeUTF(cadenaAEnviar);
				Thread.sleep(200);

			}while (!loginSuccessfull );

			// Close socket
			if (socket != null && !socket.isClosed()) {
				if (!socket.isInputShutdown()) {
					socket.shutdownInput();
				}
				if (!socket.isOutputShutdown()) {
					socket.shutdownOutput();
				}
				socket.close();
			}

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

}
