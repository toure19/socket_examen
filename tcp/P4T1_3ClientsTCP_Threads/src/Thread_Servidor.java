import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Thread_Servidor extends Thread {

	String name;
	Socket socket;

	public Thread_Servidor(String name, Socket skClient) {
		super();
		this.setName(name);
		this.socket = skClient;
	}

	public void run(){
		String data = null;
		System.out.println(this.getName()+": Attending the Client ");
		// OutputStream creation

		// Input socket creation
		InputStream in;
		try {
			in = socket.getInputStream();

			// getting data from Client socket
			DataInputStream inStream = new DataInputStream(in);
			// Printing data received
			data = inStream.readUTF();

			System.out.println("Data from Client: " + data);

			OutputStream out = socket.getOutputStream();
			// setting a new DataOutputStream to send data to Client
			DataOutputStream outStream = new DataOutputStream(out);
			// Writing on Client socket
			outStream.writeUTF(data);
			//finishing loop
			System.out.println(this.getName()+": Finishing the Client ");

			// Close socket
			if (socket != null && !socket.isClosed()) {
				if (!socket.isInputShutdown()) {
					socket.shutdownInput();
				}
				if (!socket.isOutputShutdown()) {
					socket.shutdownOutput();
				}
				socket.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}