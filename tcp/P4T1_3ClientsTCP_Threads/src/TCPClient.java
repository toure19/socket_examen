import java.io.*;
import java.net.*;
import java.util.Scanner;

public class TCPClient {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		if (args.length!= 2){
			System.out.println("Format invocació programa: java programa IPServidor portServidor");
			return;
		}
		String cadena;
		// nameServer and port
		InetAddress hostServer = InetAddress.getByName(args[0]);
		int port = Integer.parseInt(args[1]);

		// Socket creation
		Socket skClient = new Socket(hostServer, port);

		// Output socket creation
		OutputStream out = skClient.getOutputStream();
		//setting a new DataOutputStream to send data to Server 
		DataOutputStream outStream= new DataOutputStream(out);
		//Writing on Server socket
		cadena = getCadena();
		outStream.writeUTF(cadena);
		
		// Input socket creation
		InputStream in = skClient.getInputStream();
		// getting data from socket
		DataInputStream inStream = new DataInputStream(in);
		// Printing data received
		System.out.println("Data received from TCPServer: "+ inStream.readUTF());

		// Close socket
		if (skClient != null && !skClient.isClosed()) {
			if (!skClient.isInputShutdown()) {
				skClient.shutdownInput();
			}
			if (!skClient.isOutputShutdown()) {
				skClient.shutdownOutput();
			}
			skClient.close();
		}

	}
	
	// --------------------------------------------------

		static String getCadena() {
			String cadena = null;

			Scanner s = new Scanner(System.in);
			System.out.print("Cadena a enviar:");
			cadena = s.nextLine();

			return cadena;
		}

}