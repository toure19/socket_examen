import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer_Thread {

	public static void main(String[] args) throws IOException {
		if (args.length!= 1){
			System.out.println("Format invocació programa: java TCPServer_fil portServidor");
			return;
		}

		int port = Integer.parseInt(args[0]), n=0;
		Socket skClient = null;
		boolean iterar = true;
		Thread_Servidor fil = null;

		// ServerSocket creation
		ServerSocket skServidor = new ServerSocket(port);
		System.out.println("ServerTCP Listening on port " + port + "...");

		// 2 loops
		while (iterar) {
			n++;
			System.out.println("Waiting a new Client... ");
			// Socket waiting until receiving a new request
			// skClient handles the request
			skClient = skServidor.accept();
			fil = new Thread_Servidor("FilTCP"+n,skClient);
			fil.start();
			//finishing loop

			if (n == 5) iterar = false;

		}

		if (!skServidor.isClosed()) skServidor.close();

		System.out.println("Client served!");
	}
}
//TANCAMENT SERVER SOCKET