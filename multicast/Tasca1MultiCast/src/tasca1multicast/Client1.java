/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasca1multicast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author toure19_notebook
 */
public class Client extends Thread {

    private int port;

    public Client(int port) {
        this.port = port;
    }

    public synchronized void run() {

        try {
            MulticastSocket socket = new MulticastSocket(5555);
            InetAddress address = InetAddress.getByName("224.224.224.224");
            DatagramPacket packet = null;
            byte[] msg = new byte[1000];

            socket.joinGroup(address);
            for (int i = 1; i < 11; i++) {
                packet = new DatagramPacket(msg, msg.length);
                socket.receive(packet);
                System.out.println(new String(packet.getData()));
            }

            if (socket != null) {
                socket.leaveGroup(address);
                socket.close();
            }

        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
