/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasca1multicast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author toure19_notebook
 */
public class Servidor extends Thread {

    private int port = 5555;

    public Servidor(int port) {
        this.port = port;
    }

    public void run() {
        try {
            //sleep(1000);
            String msg = null;
            MulticastSocket socket = new MulticastSocket(this.port);
            InetAddress address = InetAddress.getByName("224.224.224.224");
            DatagramPacket packet = null;

            for (int i = 1; i < 11; i++) {
                msg = i + "0% completado.";
                packet = new DatagramPacket(msg.getBytes(), msg.length(), address, this.port);
                socket.send(packet);
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
