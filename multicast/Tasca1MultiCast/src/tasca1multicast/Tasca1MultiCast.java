/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasca1multicast;

/**
 *
 * @author toure19_notebook
 */
public class Tasca1MultiCast {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int port = 5555;
        Servidor servidor = new Servidor(port);
        Client client1 = new Client(port);
        Client client2 = new Client(port);

        client1.setName("CLIENT1");
        client2.setName("CLIENT2");
        servidor.setName("SERVIDOR");
        servidor.start();
        client1.start();
        client2.start();
    }

}
