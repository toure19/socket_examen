/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael
 */
public class Server extends Thread {

    //Atributs del server
    ServerSocket serverSocket = null;
    Socket socketClient;
    DataInputStream in;
    DataOutputStream out;
    String msj = "";
    public boolean salir = false;
    int port = 7777;//Ens la juguem al 7 de la sort xD

    //run
    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("[SERVER] Inicat....");

            while (!salir) { //---->condicio de eixida booleana
                socketClient=serverSocket.accept();//---->esperem peticions
                System.out.println("[SERVER]Conexio amb el client acceptada");
                Conexio conexio=new Conexio(socketClient);//--->Creem el fil
                conexio.start();//---->El iniciem 
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
