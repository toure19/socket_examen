/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael
 */
public class Conexio extends Thread {

    //Atributs
    Socket socket;
    DataInputStream in;
    DataOutputStream out;
    String msj = "";
    public boolean salir = false;
    ArrayList<Pelicula> pelicules;
    //Consrtructor

    public Conexio(Socket socket) throws IOException {
        this.socket = socket;//--->li pasem el socketr al constructor
        in = new DataInputStream(socket.getInputStream());
        out = new DataOutputStream(socket.getOutputStream());
    }

    public Conexio() {
    }

    //run
    public void run() {
        try {
            System.out.println("[SERVER CONEXIO] Se ha iniciat un intercambi");
            pelicules = Pelicules.getPelicules();
            while (!salir) {
                try {
                    //DONEM A TRIAR LA PELICULA
                    System.out.println("[SERVER CONEXIO] Enviem les pelicules al client");
                    out.writeUTF(pelicules.toString());
                    System.out.println("[SERVER CONEXIO] Esperant la resposta....");
                    msj = in.readUTF();
                    System.out.println("[SERVER CONEXIO] la resposta es " + msj);
                    Pelicula peliculaTriada = Pelicules.retornaPelicula(Integer.parseInt(msj));//----->Recuperem la pelicula triada convertint el msj en integer, Altra opcio es crear un buscar per nom...
                    //DONEM A TRIAR LA SESSIO
                    Sessions sessions = new Sessions(peliculaTriada.getSessionsPeli());
                    System.out.println("[SERVER CONEXIO] Donem a triar la sessio...");
                    out.writeUTF(sessions.toString());
                    System.out.println("[SERVER CONEXIO] sessions.toString " + sessions.toString() );
                    System.out.println("[SERVER CONEXIO] Esperant la resposta....");
                    msj = in.readUTF();
                    System.out.println("[SERVER CONEXIO] la sessio es " + msj);
                    Sessio sessio = sessions.getSessions().get(Integer.parseInt(msj));
                    Seient[][] seients = sessio.getSeients();
                    System.out.println("[SERVER CONEXIO] Donem a triar els seients...");
                    out.writeUTF(sessio.mapaSessioString());
                    int x=in.readInt();
                    int y=in.readInt();
                    //TODO reservar els seients
                    
                    
                    if (msj.contains("fin")) {
                        salir = true;
                    }
                    
                    salir=true;
                    
                } catch (IOException ex) {
                    Logger.getLogger(Conexio.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            out.close();
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(Conexio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
