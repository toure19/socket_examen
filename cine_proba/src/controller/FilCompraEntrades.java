package controller;

import model.Seient;
import model.Pelicula;
import model.Sala;
import model.Pelicules;
import model.Sessio;
import java.math.BigDecimal;
import java.util.ArrayList;

public class FilCompraEntrades implements Runnable {
	String nom;

	public FilCompraEntrades(String nom) {
		this.nom = nom;
	}

	// ----------
	public void run() {

		Pelicula p = null;
		Sala sa = null;
		Sessio se = null;
		//SeientsReservats seientsreservats = new SeientsReservats();

		int sessio, pelicula, reserva;

		Pelicules.llistarPelicules();
		pelicula = Validacio.validaSencer("["+this.nom + "] \t Tria PELICULA:",Pelicules.quantitatPelicules());
		p = Pelicules.retornaPelicula(pelicula);

		p.llistarSessionsPeli();
		sessio = Validacio.validaSencer("["+this.nom + "] \t Tria la sessió per a "+ p.getNomPeli() + ":", p.getSessionsPeli().size());

		se = p.retornaSessioPeli(sessio);
		sa = se.getSala();
		se.mapaSessio();

		reserva = Validacio.validaSencer("["+this.nom + "] \t Quantes entrades vols?: ", 100);

		try {
			reserva_numEntrades(p, se, sa, reserva);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		se.mapaSessio();

	}

	//---------------------
	//metode que tracta de reservar totes les entrades sol·licitades, 
	// retorna TRUE  -> si es reserven TOTES les entrades
	// retorna FALSE -> si NO s'ha pogut reservar alguna entrada, aleshores no es reservarà CAP
	public static void reserva_numEntrades(Pelicula p, Sessio se, Sala sa, int numEntrades) throws InterruptedException{
		boolean isReservat = true;
		int fila, seient;
		//ArrayList de la quantitat de seients que es volen comprar
		ArrayList<Seient> seientsAcomprar = new ArrayList<Seient>();

		for (int i=0; i < numEntrades; i++){
			System.out.println("\tSeient "+(i+1)+" :");
			fila = Validacio.validaSencer("\t\t Tria FILA: [1-"+sa.getFiles()+"] ",sa.getFiles());
			seient = Validacio.validaSencer("\t\t Tria SEIENT en la fila: [1-"+sa.getTamanyFila()+"]",sa.getTamanyFila());		
			Seient[][] seients = se.getSeients();
			if (seients[fila-1][seient-1].verificaSeient()){ //Reserva SEIENT
				seients[fila-1][seient-1].reservaSeient();	
				seientsAcomprar.add(seients[fila-1][seient-1]);//afegeix seient a llista SEIENTS RESERVATS
			}else{ //NO Reserva
				System.out.println("\t ERROR Cine:validaSeient: Seient reservat/ocupat");
				isReservat = false;
			};
			System.out.println("\tSeguent Seient...");
		}//endfor

		if (isReservat){ //Compra seients
			System.out.println("\nSEIENT RESERVATS: "+seientsAcomprar.size());
			pagamentEntrada(new BigDecimal(numEntrades).multiply(se.getPreu()));
			for (int i=0; i < seientsAcomprar.size(); i++){
				Seient s = seientsAcomprar.get(i);
				s.ocupaSeient(); 		//ocupa seient
				se.imprimirTicket(s,se, sa, p);
				System.out.println();
			}//for
		}else{// Llibera seients
			System.out.println("\t\tNO sha pogut fer la compra de "+numEntrades+" entrades. Es queden Lliures");
			for (int i=0; i < seientsAcomprar.size(); i++){
				Seient s = seientsAcomprar.get(i);
				s.alliberaSeient(); 		//ocupa seient
			}//for
		}
		for (int i=seientsAcomprar.size(); i > 0; i--)
			seientsAcomprar.remove(i-1); //elimina seient de la llista

	}

	//*********************************************************
	//PAGAMENT D'UNA ENTRADA
	static boolean pagamentEntrada(BigDecimal preu){
		System.out.println("Import a pagar: "+preu);
		System.out.println("\nPagant...(2seg)");
		//pagant
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Validacio.validaBoolea("Pagat? (S/N)");

	}
	// ----------------------------------------
/*
	public void validaSeientsNoInteractiu(Pelicula p, Sessio se, Sala sa, int reserva, int f1, int s1) throws InterruptedException {
		boolean isreservat = true;
		int fila, seient;
		//ArrayList de la quantitat de seients que es volen comprar
		ArrayList<Seient> seientsAcomprar = new ArrayList<Seient>();

		for (int i = 0; i < reserva; i++) {
			System.out.println(this.nom + "\tSeient " + (i + 1));
			fila = Validacio.validaSencer(this.nom + "\t\t Tria FILA: [1-" + sa.getFiles() + "] ",	sa.getFiles());
			seient = Validacio.validaSencer(this.nom+ "\t\t Tria SEIENT en la fila: [1-" + sa.getTamanyFila()+ "]", sa.getTamanyFila());
			Seient[][] seients = se.getSeients();

			// SINCRONITZACIÓ DE CODI
			// ///////////////////////////////////////////////
			synchronized (this) {

				if (seients[fila - 1][seient - 1].verificaSeient()) { // Reserva  SEIENT
					seients[fila - 1][seient - 1].reservaSeient(); // afegeix seient a llista SEIENTS RESERVATS
					seientsreservats.afegirSeient(seients[fila - 1][seient - 1]);
				} else { // NO Reserva
					isreservat = false;
					System.out.println(this.nom+ "\t ERROR Cine:validaSeient: Seient reservat/ocupat");
				}// else

			}
			// SYNCHRONIZED
			// ////////////////////////////////////////////////////////////////////////////
		}// for

		if (isreservat) { // Compra seients
			System.out.println(this.nom + "\nSEIENT OCUPATS: "+ seientsreservats.quantitatSeientsReservats());

			Cine.pagamentEntrada(this.nom + "\t Pagant " + reserva	+ " entrades\n\n");
			for (int i = 0; i < seientsreservats.quantitatSeientsReservats(); i++) {
				Seient s = seientsreservats.retornaSeient(i);
				s.ocupaSeient(); // ocupa seient
				se.imprimirTicket(s, se, sa, p);
				System.out.println();
			}// for
		} else {// Llibera seients
			System.out.println(this.nom + "\t\tNO sha pogut fer la compra de "+ reserva + " entrades");
			for (int i = 0; i < seientsreservats.quantitatSeientsReservats(); i++) {
				Seient s = seientsreservats.retornaSeient(i);
				s.alliberaSeient(); // ocupa seient
			}// for
		}// else
		for (int i = seientsreservats.quantitatSeientsReservats(); i > 0; i--) {
			seientsreservats.esborraSeient(i - 1); // elimina seient de la llista

		}// for
	}
*/
}// class