/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clientes;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Rafael
 */
public class Cliente {

    static Scanner tc = new Scanner(System.in);
    //Atributos
    static String host = "localhost";
    static String msj = "";
    static Socket socket;
    static int port = 7777;
    static DataInputStream in;
    static DataOutputStream out;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        //variables
        boolean salir = false;

        socket = new Socket(host, port);
        in = new DataInputStream(socket.getInputStream());
        out = new DataOutputStream(socket.getOutputStream());
       // while (!salir) {
            msj = in.readUTF();
            System.out.println("[CLIENTE] peliculas a elegir:" + msj);
            msj = tc.nextLine();
            System.out.println("[CLIENTE] voy a enviar " + msj);
            out.writeUTF(msj);
            msj = in.readUTF();
            System.out.println("[CLIENTE] sessiones a elegir:" + msj);
            msj = tc.nextLine();
            System.out.println("[CLIENTE] voy a escoger " + msj);
            out.writeUTF(msj);
            msj = in.readUTF();
            System.out.println("[CLIENTE] asientos a elegir:" + msj);
            System.out.println("Seleccionamos fila");
            msj = tc.nextLine();
            out.writeUTF(msj);
            System.out.println("Seleccionamos columna");
            msj = tc.nextLine();
            out.writeUTF(msj);
            //salir = true;
        //}
        socket.close();

    }

}
