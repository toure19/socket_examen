import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		/////////////////////////////////////////////////////////////////////////////////
		String cadenaAEnviar, cadenaRebuda;
		int portOnEnviar = 9999;
		int portEscolta = 9000;
		boolean usuariValidat = false;
		DatagramPacket resp;
		try {
			Scanner s = new Scanner(System.in);
			DatagramSocket socket1 = new DatagramSocket();

			System.out.println("CLIENT port del servidor: "+portOnEnviar);
			//Getting USERNAME to SERVER   *************************************************************
			System.out.print("Usuari a enviar al servidor: ");
			//missage to SEND 
			cadenaAEnviar=s.nextLine()+ " ";

			//System.out.println("Cadena a enviar pel CLIENT: "+ cadenaAEnviar);
			InetAddress desti = InetAddress.getLocalHost();

			//New packet and sending it -> Sending USERNAME >>>>>>>>>>>>>>>>
			DatagramPacket req = new DatagramPacket(cadenaAEnviar.getBytes(),cadenaAEnviar.getBytes().length,desti,portOnEnviar);
			socket1.send(req);


			//Getting reponse from SERVER *************************************************************
			DatagramSocket socket2 = new DatagramSocket(portEscolta);  //<------   port LISTEN at  9000
			
			//Loop until valid username
			do {	
				//buffer to read response
				byte missatge[] = new byte[1000];
				//New packet -> Receiving response and switching cases... 
				resp = new DatagramPacket(missatge,missatge.length);
				socket2.receive(resp);
				cadenaRebuda = new String(resp.getData());
				
				//System.out.println("Cadena enviada pel SERVIDOR: "+cadenaRebuda);
				if (cadenaRebuda.contains("NO existeix")) { //WRONG username identified
					System.out.println(">"+cadenaRebuda);
					//Asking for sending new attempt
					System.out.print("Proporciona un usuari vàlid: ");
					cadenaAEnviar = s.nextLine()+" \n";
					//Sending a new packet *****************************************************
					req = new DatagramPacket(cadenaAEnviar.getBytes(),cadenaAEnviar.getBytes().length,desti,portOnEnviar);
					socket1.send(req);

				}else {	//RIGHT username identified, so asking for PASSWORD
					System.out.print("Contrassenya per usuari "+ cadenaAEnviar.substring(0, cadenaAEnviar.length()-2) +": ");
					usuariValidat = true;
					cadenaAEnviar = "PASSWD "+ s.nextLine()+" \n";	
				}
			}while(!usuariValidat);

			
			//System.out.println("Cadena a enviar pel CLIENT:"+cadenaAEnviar);
			
			//missatge = cadenaAEnviar.getBytes();
			//Sending a new packet, PASSWORD to check  *************************************************************
			req = new DatagramPacket(cadenaAEnviar.getBytes(),cadenaAEnviar.getBytes().length,desti,portOnEnviar);
			socket1.send(req);

			//Receiving a new packet,LOGIN checked   ******************************************************
			byte missatge[] = new byte[1000];
			resp = new DatagramPacket(missatge,missatge.length);
			socket2.receive(resp);

			System.out.println(">"+new String(resp.getData()));

			//Tanquem socket d'enviament
			if(!socket1.isClosed())
				socket1.close();

			if(!socket2.isClosed())
				socket2.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
