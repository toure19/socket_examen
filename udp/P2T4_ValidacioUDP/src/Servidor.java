import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.Scanner;

public class Servidor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/////////////////////////////////////////////////////////////////////////////////
		String cadenaAEnviar, cadenaRebuda, cadenaRebudaParticionada [], contrassenya;
		String ValidUser = "laura", ValidPassword="l123";
		boolean loginSuccessfull = false;
		int portEnvia = 9000;
		int portEscolta = 9999;
		int TTL = 20000;
		DatagramSocket socket1,socket2;

		try {
			Scanner s = new Scanner(System.in);
			System.out.println("SERVIDOR escoltant al port "+ portEscolta);
			long tempsInicial = System.currentTimeMillis();
			
			socket1 = new DatagramSocket(portEscolta);  //<--- LISTEN port 9999
			socket1.setSoTimeout(TTL);
			//buffer to read response
			byte missatge[] = new byte[1000];

			do {
				//Getting response from CLIENT *************************************************************
				//New packet -> Receiving response 
				DatagramPacket resp = new DatagramPacket(missatge,missatge.length);
				socket1.receive(resp);
				cadenaRebuda = new String(resp.getData());
				System.out.println("Cadena enviada pel CLIENT: "+cadenaRebuda);
					
	/*			cadenaRebudaParticionada = cadenaRebuda.split(" ");
				for (int i=0; i<cadenaRebudaParticionada.length; i++)
					System.out.println("cadenaRebudaParticionada["+i+"]: "+cadenaRebudaParticionada[i] +" tamany cadena: "+cadenaRebudaParticionada[i].length());
	*/			
				if (!cadenaRebuda.contains("PASSWD")) {	//Obtaining USERNAME
					//Split data received and get USERNAME -> 1st vector = [0]
					if (cadenaRebuda.split(" ")[0].equals(ValidUser)) {  //username NOT EXISTS!!
						cadenaAEnviar = "Usuari existeix \n";

					}else {	//username EXISTS!!
						cadenaAEnviar = "Usuari NO existeix \n";
						}
					//System.out.println(cadenaAEnviar);
					
				}else { //Obtaining PASSWORD
					//Split data received and get PASSWORD  -> 2nd vector = [1]
					contrassenya = cadenaRebuda.split(" ")[1];
					//System.out.println(contrassenya);
					
					if (contrassenya.equals(ValidPassword)) {//RIGHT password
						cadenaAEnviar = "Usuari "+ValidUser+" validat correctament";
						loginSuccessfull = true;
					}else 	//WRONG password
						cadenaAEnviar = "Usuari "+ValidUser+" NO validat, contrassenya incorrecta";
				}
				//System.out.println("Cadena a enviar pel SERVIDOR: "+ cadenaAEnviar);
				//missatge = new byte[1000];
				//missatge a ENVIAR 
				//byte[] missatgeEnviar =  cadenaAEnviar.getBytes();
				InetAddress desti = InetAddress.getLocalHost();

				//New packet and sending it -> Sending validation of PASSWORD >>>>>>>>>>>>>>>>
				socket2 = new DatagramSocket();
				DatagramPacket req = new DatagramPacket(cadenaAEnviar.getBytes(),cadenaAEnviar.getBytes().length,desti,portEnvia);
				socket2.send(req);
				Thread.sleep(200);

			}while (System.currentTimeMillis() - tempsInicial < TTL || !loginSuccessfull );
			
			if(!socket1.isClosed())
				socket1.close();
			
			if(!socket2.isClosed())
				socket2.close();

			System.out.println("\t\t["+Thread.currentThread().getName()+"] "+ "Tancant servidor passats 10seg: ");

		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			if(e instanceof SocketTimeoutException) 
				System.out.println("Timeout!!! Excepció pel venciment del temporitzador de "+ TTL/1000 + " segs");
			else
				e.printStackTrace();
		}
	}

}
